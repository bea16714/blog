<?php

namespace App\Models;

class Article
{
    private $db;

    public function __construct ($db) {
        $this->db = $db;
    }

    public function create($cover, $title, $shortDescription, $detailedText, $url): bool
    {

        return $this->db->query("INSERT INTO `articles` (`id`, `cover`, `title`, `shortDescription`, `detailedText`, `url`) VALUES (NULL, '$cover', '$title', '$shortDescription', '$detailedText', '$url');");

    }

    public function get($url): array
    {

        $result = $this->db->query("SELECT * FROM `articles` WHERE `url` = '$url'");
        $row = $result->fetch_assoc();

        return [
            'title' => $row['title'],
            'id' => $row['id'],
            'cover' => $row['cover'],
            'shortDescription' => $row['shortDescription'],
            'detailedText' => $row['detailedText'],
            'url' => $row['url']
        ];

    }

    public function getList(): array
    {

        $result = $this->db->query("SELECT * FROM `articles`");
        $arr = [];

        if (mysqli_num_rows($result) > 0) {
            while ($row = $result->fetch_assoc()){
                array_push($arr, ['id' => $row['id'], 'title' => $row['title'], 'shortDescription' => $row['shortDescription'], 'url' => $row['url']]);
            }
        }

        return $arr;

    }

    public function update($url, $textUpdate): bool
    {

        $result = $this->db->query("DESCRIBE articles");
        while ($row = $result->fetch_assoc()) {
            $db[] = $row['Field'];
        }
        $text = "";

        foreach ($textUpdate as $key => $field) {
            if (!empty($field)) {
                if(in_array($key, $db)){

                    $text .= "`$key`='$field',";

                }

            }
        }
        $text = substr($text,0,-1);

        return $this->db->query("UPDATE `articles` SET $text WHERE `articles`.`url` = $url;");

    }

    public function delete($url): bool
    {

        return $this->db->query("DELETE FROM `articles` WHERE `articles`.`url` = '$url'");

    }

}
