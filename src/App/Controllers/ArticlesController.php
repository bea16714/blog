<?php

namespace App\Controllers;

class ArticlesController
{
    private $db;
    private $view;
    private $art;

    public function __construct ($db, $view, $art) {
        $this->db = $db;
        $this->view = $view;
        $this->art = $art;
    }

    public function post($id){

        $get = $this->art->get($id);
        $body = $this->view->render('post.twig', [
            'title' => $get['title'],
            'id' => $get['id'],
            'cover' => $get['cover'],
            'shortDescription' => $get['shortDescription'],
            'detailedText' => $get['detailedText'],
            'url' => $get['url']
        ]);

        echo $body;

    }

    public function allPosts(){

        $posts = $this->art->getList();
        $body = $this->view->render('all_entries.twig', [
            'posts' => $posts
        ]);

        echo $body;

    }

    public function update($url){

        $get = $this->art->get($url);
        $body = $this->view->render('update.twig', [
            'title' => $get['title'],
            'id' => $get['id'],
            'cover' => $get['cover'],
            'shortDescription' => $get['shortDescription'],
            'detailedText' => $get['detailedText'],
            'url' => $get['url']
        ]);

        echo $body;

    }

    public function addPost(){

        $body = $this->view->render('addPost.twig');

        echo $body;

    }

    public function delete($url){

        $delete = $this->art->delete($url);
        if ($delete == false)
            $body = $this->view->render('delete.twig', ['response' => 'Ошибочка(']);
        else
            $body = $this->view->render('delete.twig', ['response' => 'Запись удалена!']);

        echo $body;

    }

    public function greetings(){

        $body = $this->view->render('greetings.twig');

        echo $body;

    }
}
