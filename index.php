<?php
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use App\Models as Models;
use App\Controllers as Controllers;

require_once "src/Utils/Database.php";
require_once "vendor/autoload.php";
include "src/autoload.php";

$loader = new FilesystemLoader('src/App/Views');
$view = new Environment($loader);

//new Models\Article;
$art = new Models\Article($mysqli);
$controller = new Controllers\ArticlesController($mysqli, $view, $art);

echo "id=$_GET[id]<br>";
echo "post=$_GET[post]";

if ($_GET['id'] == 'add') {

    $controller->addPost();

}

if($_GET['id'] == 'article') {

    $controller->post($_GET['post']);

}

if($_GET['id'] == 'update') {

    $controller->update($_GET['post']);

}

if($_GET['id'] == 'delete') {

    $controller->delete($_GET['post']);

}

if($_GET['id'] == 'list') {

    $controller->allPosts();

}

if ($_GET['id'] == '') {

    $controller->greetings();

}
